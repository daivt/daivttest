import axios from "axios";

const request = axios.create({});
const api = (options: any) => {
  return request({
    ...options,
    headers: { "Accept-Language": "vi", ...options.headers },
  });
};

export default api;
