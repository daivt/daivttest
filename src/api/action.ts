import api from ".";
import { some } from "../layout/elements";

export const getSearchResult = (params: some) => {
  return api({
    method: "get",
    url: "https://avl-frontend-exam.herokuapp.com/api/users/all",
    params,
  });
};

export const getFollowing = (params: some) => {
  return api({
    method: "get",
    url: "https://avl-frontend-exam.herokuapp.com/api/users/friends",
    params,
  });
};

export const getTags = (params?: some) => {
  return api({
    method: "get",
    url: "https://avl-frontend-exam.herokuapp.com/api/tags",
    params,
  });
};
