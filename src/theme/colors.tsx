import colors from "./colors.module.scss";

export const {
  TEAL,
  PURPLE,
  BLACK_90,
  GREY,
  WHITE,
  BLUE,
  GREEN,
  RED,
  BROWN,
  ORANGE,
  YELLOW,
  PINK,
  PRIMARY,
  BACK_GROUND,
  GRAY_SCALE,
} = colors;
