import { createTheme } from "@mui/material";
import { BACK_GROUND, ORANGE, PRIMARY, WHITE, YELLOW } from "./colors";

export const MUI_THEME = createTheme({
  palette: {
    primary: {
      main: PRIMARY,
    },
    text: {
      primary: PRIMARY,
    },
  },
  typography: {
    allVariants: {
      color: PRIMARY,
    },
    fontFamily: "Ubuntu",
    caption: {
      fontSize: 12,
      lineHeight: "18px",
      fontWeight: 400,
    },
    body1: {
      fontSize: 16,
      lineHeight: "24px",
      fontWeight: 400,
    },
    body2: {
      fontSize: 14,
      lineHeight: "21px",
      fontWeight: 400,
    },
    subtitle1: {
      fontSize: 16,
      lineHeight: "24px",
      fontWeight: 700,
    },
    subtitle2: {
      fontSize: 14,
      lineHeight: "21px",
      fontWeight: 700,
    },
    h6: {
      fontSize: 24,
      lineHeight: "36px",
      fontWeight: 400,
    },
    h5: {
      fontSize: 30,
      lineHeight: "45px",
      fontWeight: 400,
    },
    h4: {
      fontSize: 48,
      lineHeight: "72px",
      fontWeight: 700,
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          height: 40,
          borderRadius: 4,
          fontWeight: "bold",
        },
        outlined: {
          backgroundColor: BACK_GROUND,
          color: WHITE,
          border: `1px solid ${WHITE}`,
          "&:hover": {
            backgroundColor: WHITE,
            color: BACK_GROUND,
          },
        },
        contained: {
          backgroundColor: WHITE,
          color: BACK_GROUND,
          border: `1px solid ${BACK_GROUND}`,
          "&:hover": {
            backgroundColor: BACK_GROUND,
            color: WHITE,
            border: `1px solid ${WHITE}`,
          },
        },
        sizeLarge: {
          fontSize: 14,
          fontWeight: 700,
        },
        sizeSmall: {
          height: 29,
          padding: "8px 10px",
          fontWeight: 700,
          fontSize: 12,
          borderRadius: 20,
        },
      },
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          height: 60,
          border: "3px solid rgba(255, 255, 255, 0.5)",
          borderRadius: 6,
          padding: "20px 18px",
          "&.Mui-focused": {
            border: `3px solid ${ORANGE}`,
          },
        },
        input: {
          fontSize: 14,
        },
      },
    },
    MuiSlider: {
      styleOverrides: {
        track: {
          height: 8,
          background:
            "linear-gradient(90deg, rgba(255,155,51,1) 0%, rgba(255,208,93,1) 100%)",
          border: "none",
        },
        rail: {
          height: 8,
        },
        thumb: {
          height: 24,
          width: 24,
          borderRadius: "50%",
          border: `6px solid ${YELLOW}`,
          backgroundColor: BACK_GROUND,
        },
        mark: {
          visibility: "hidden",
        },
        markLabel: {
          color: "white",
        },
      },
    },
    MuiTab: {
      styleOverrides: {
        root: {
          textTransform: "none",
          color: "#929292",
          height: 64,
          paddingBottom: 12,
          paddingTop: 32,
          fontSize: 16,
          "&.Mui-selected": {
            fontWeight: 700,
          },
        },
      },
    },
    MuiCircularProgress: {
      styleOverrides: {
        root: {
          color: ORANGE,
        },
      },
    },
  },
});
