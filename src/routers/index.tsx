import { RouteObject } from "react-router-dom";
import HomeLayout from "../layout/HomeLayout";
import Home from "../modules/home/page";
import Results from "../modules/results/page";
import Tags from "../modules/tags/page";

export const listRoute: RouteObject[] = [
  {
    path: "/",
    element: <HomeLayout />,
    children: [
      { index: true, element: <Home /> },
      {
        path: "results",
        element: <Results />,
      },
      { path: "*", element: <Home /> },
    ],
  },
  {
    path: "/tags",
    element: <Tags />,
  },
];

export const listRouteLabel = [
  {
    path: "/",
    title: "Home",
    subMenu: [
      {
        path: "/results",
        title: "Results",
      },
    ],
  },
  {
    path: "/tags",
    title: "Tags",
  },
];
