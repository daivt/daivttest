export { ReactComponent as LogoIcon } from "./logo.svg";
export { ReactComponent as BackIcon } from "./back.svg";
export { ReactComponent as MenuActive } from "./menuActive.svg";
export { ReactComponent as MenuInactive } from "./menuInactive.svg";
