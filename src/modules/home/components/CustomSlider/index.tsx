import { Slider } from "@mui/material";
import React from "react";

const marks = [
  {
    value: 0,
    label: 3,
    trueValue: 3,
  },
  {
    value: 20,
    label: 6,
    trueValue: 6,
  },
  {
    value: 40,
    label: 9,
    trueValue: 9,
  },
  {
    value: 60,
    label: 12,
    trueValue: 12,
  },
  {
    value: 80,
    label: 15,
    trueValue: 15,
  },
  {
    value: 100,
    label: 50,
    trueValue: 50,
  },
];

const getValue = (val?: number) => {
  return marks.some((v) => v.value === val)
    ? marks.find((v) => v.value === val)?.trueValue.toString() || ""
    : "";
};

interface Props {
  onChangeCommitted: (val?: number) => void;
  value: number;
}
const CustomSlider: React.FC<Props> = (props) => {
  const { onChangeCommitted, value } = props;
  const [slideVal, setSlideVal] = React.useState(0);
  React.useEffect(() => {
    const newVal = marks.find((v) => v.value === value)?.value;
    if (!!newVal) {
      setSlideVal(newVal);
    }
  }, [value]);
  return (
    <Slider
      step={20}
      marks={marks}
      value={slideVal}
      getAriaValueText={getValue}
      onChange={(e, val) => setSlideVal(Number(val))}
      onChangeCommitted={(e, val) => {
        setSlideVal(Number(val));
        onChangeCommitted(marks.find((v) => v.value === val)?.trueValue);
      }}
    />
  );
};

export default CustomSlider;
