import { Button, Divider, InputBase, Typography } from "@mui/material";
import queryString from "query-string";
import React from "react";
import { Link } from "react-router-dom";
import { Col } from "../../../layout/elements";
import CustomSlider from "../components/CustomSlider";
import styles from "./index.module.scss";

const Home = () => {
  const [filter, setFilter] = React.useState({
    page: 1,
    pageSize: 3,
    keyword: "",
  });

  return (
    <Col className={styles.main}>
      <Typography variant="h6" className={styles.homeTitle}>
        Search
      </Typography>
      <InputBase
        placeholder="Keyword"
        value={filter.keyword}
        onChange={(e) =>
          setFilter({ ...filter, keyword: e.target.value.trim() })
        }
      />
      <Divider className={styles.divider} />
      <Typography variant="h6" className={styles.homeTitle}>
        # Of Results Per Page
      </Typography>
      <Typography variant="body1" className={styles.homeLabel}>
        <Typography variant="h4" component="span">
          {filter.pageSize}
        </Typography>
        &nbsp; results
      </Typography>
      <CustomSlider
        value={filter.pageSize}
        onChangeCommitted={(val: number | undefined) =>
          val && setFilter({ ...filter, pageSize: val })
        }
      />
      <Divider className={styles.dividerBottom} />
      <Link
        className="link-non-decoration"
        to={{ pathname: "/results", search: queryString.stringify(filter) }}
      >
        <Button variant="contained" className={styles.searchButton}>
          Search
        </Button>
      </Link>
    </Col>
  );
};

export default Home;
