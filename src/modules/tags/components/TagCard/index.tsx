import { Typography } from "@mui/material";
import React from "react";
import { Col, some } from "../../../../layout/elements";
import styles from "./index.module.scss";

interface Props {
  data: some;
}
const TagCard: React.FC<Props> = (props) => {
  const { data } = props;
  return (
    <Col className={styles.main}>
      <Col className={styles.tag}>
        <div className={styles.tagName}>
          <Typography color="primary" variant="h6" className={styles.tagText}>
            {data?.name}
          </Typography>
        </div>
      </Col>
      <Typography className={styles.nameCard} variant="subtitle2">
        {data?.name}
      </Typography>
      <Typography variant="caption">{data?.count} Results</Typography>
    </Col>
  );
};

export default TagCard;
