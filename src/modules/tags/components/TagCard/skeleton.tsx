import { Skeleton } from "@mui/material";
import React from "react";
import { Col } from "../../../../layout/elements";
import styles from "./index.module.scss";

const TagCardSkeleton = () => {
  return (
    <Col className={styles.main}>
      <Skeleton
        variant="rectangular"
        height={150}
        style={{ marginBottom: 10, borderRadius: 8 }}
      />
      <Skeleton variant="text" />
      <Skeleton variant="text" />
    </Col>
  );
};

export default TagCardSkeleton;
