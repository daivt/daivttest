import { Typography } from "@mui/material";
import React from "react";
import { getTags } from "../../../api/action";
import { Col, Row, some } from "../../../layout/elements";
import TagCard from "../components/TagCard";
import TagCardSkeleton from "../components/TagCard/skeleton";
import styles from "./index.module.scss";

const Tags = () => {
  const [data, setData] = React.useState<some[] | null>(null);
  const [loading, setLoading] = React.useState(false);
  const getData = React.useCallback(async () => {
    setLoading(true);
    const res = await getTags();
    if (res?.status === 200) {
      setData(res?.data);
    }
    setLoading(false);
  }, []);

  React.useEffect(() => {
    getData();
  }, [getData]);

  return (
    <Col className={styles.main}>
      <Col className={styles.container}>
        <Typography variant="h5" color="primary" className={styles.title}>
          Tags
        </Typography>
        <Row className={styles.gridRow}>
          {data?.map((item, index) => (
            <TagCard key={index} data={item} />
          ))}
          {loading &&
            Array(10)
              .fill(0)
              .map((v, index) => <TagCardSkeleton key={index} />)}
        </Row>
      </Col>
    </Col>
  );
};

export default Tags;
