import { Avatar, Skeleton, Typography } from "@mui/material";
import React from "react";
import { Col, some } from "../../../../layout/elements";
import styles from "./index.module.scss";
interface Props {
  data: some;
}
const ResultCard: React.FC<Props> = (props) => {
  const { data } = props;
  if (data.skeleton) {
    return (
      <Col className={styles.mainCard}>
        <Avatar className={styles.avatar} variant="square" />
        <Typography variant="subtitle2">
          <Skeleton width="120px" />
        </Typography>
        <Typography variant="caption">
          <Skeleton width="80px" />
        </Typography>
      </Col>
    );
  }
  return (
    <Col className={styles.mainCard}>
      <Avatar
        src={data?.avater}
        alt={data?.name}
        className={styles.avatar}
        variant="rounded"
      />
      <Typography variant="subtitle2">{data?.name}</Typography>
      <Typography variant="caption">{data?.username}</Typography>
    </Col>
  );
};

export default ResultCard;
