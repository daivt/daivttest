import { CircularProgress } from "@mui/material";
import React from "react";
import { Col } from "../../../../layout/elements";
import styles from "./index.module.scss";

const Loading = () => {
  return (
    <Col className={styles.main}>
      <CircularProgress />
    </Col>
  );
};

export default Loading;
