import LoadingButton from "@mui/lab/LoadingButton";
import { Typography, useMediaQuery } from "@mui/material";
import queryString from "query-string";
import React from "react";
import { Link, useLocation } from "react-router-dom";
import { getSearchResult } from "../../../api/action";
import { BackIcon } from "../../../assets";
import { MOBILE_WIDTH } from "../../../constants";
import { Col, Row, some } from "../../../layout/elements";
import ResultCard from "../components/ResultCard";
import styles from "./index.module.scss";

const Results = () => {
  const location = useLocation();
  const [data, setData] = React.useState<some[]>([]);
  const [total, setTotal] = React.useState<number>(0);
  const [filter, setFilter] = React.useState<some | undefined>(undefined);
  const [loading, setLoading] = React.useState(false);

  const isMobile = useMediaQuery(MOBILE_WIDTH);

  const handleQueryParams = React.useCallback(() => {
    let params: some = { page: 1, pageSize: 3, keyword: "" };
    const paramsQuery = queryString.parse(location.search);
    if (!!paramsQuery.page) {
      params.page = Number(paramsQuery.page);
    }
    if (!!paramsQuery.pageSize) {
      params.pageSize = Number(paramsQuery.pageSize);
    }
    if (!!paramsQuery.keyword) {
      params.keyword = paramsQuery.keyword;
    }
    setFilter(params);
  }, [location.search]);

  const getData = React.useCallback(async () => {
    if (!filter) {
      return;
    }
    setLoading(true);
    const res = (await getSearchResult(filter)) as any;
    if (res?.status === 200) {
      setData((old) => [...old, ...res?.data?.data]);
      setTotal(res?.data?.total || 0);
    }
    setLoading(false);
  }, [filter]);

  const addMore = React.useCallback(() => {
    setFilter({ ...filter, page: filter?.page + 1 });
  }, [filter]);

  React.useEffect(() => {
    handleQueryParams();
  }, [handleQueryParams]);

  React.useEffect(() => {
    getData();
  }, [getData]);

  return (
    <Col className={styles.container}>
      {isMobile ? (
        <Typography variant="h5" style={{ marginBottom: 24 }}>
          Results
        </Typography>
      ) : (
        <Link
          to={{ pathname: "/" }}
          className="link-non-decoration"
          style={{ marginBottom: 24 }}
        >
          <Row className={styles.backRow}>
            <BackIcon className={styles.backIcon} />
            <Typography variant="h5">Results</Typography>
          </Row>
        </Link>
      )}

      <Row className={styles.resultContainer}>
        {data?.map((result, index) => (
          <ResultCard data={result} key={index} />
        ))}
        {loading &&
          Array(6)
            .fill({ skeleton: true })
            ?.map((result, index) => <ResultCard data={result} key={index} />)}
      </Row>
      {data.length < total && (
        <LoadingButton
          loading={loading}
          variant="contained"
          className={styles.buttonMore}
          onClick={addMore}
          disabled={loading}
        >
          More
        </LoadingButton>
      )}
    </Col>
  );
};

export default Results;
