import { Typography } from "@mui/material";
import React from "react";
import { Link, useLocation } from "react-router-dom";
import { BackIcon, LogoIcon } from "../../../assets";
import { Row } from "../../elements";
import styles from "./index.module.scss";

const MobileHeader = () => {
  const location = useLocation();
  const isHome = location.pathname === "/";
  return (
    <Row className={styles.main}>
      {isHome ? (
        <LogoIcon className={styles.backRow} />
      ) : (
        <Link to={{ pathname: "/" }} className="link-non-decoration">
          <Row className={styles.backRow}>
            <BackIcon className={styles.backIcon} />
            <Typography variant="h6">Home Page</Typography>
          </Row>
        </Link>
      )}
    </Row>
  );
};

export default MobileHeader;
