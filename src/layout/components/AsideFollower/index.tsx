import { Tab, Tabs } from "@mui/material";
import React from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { getFollowing, getSearchResult } from "../../../api/action";
import { some } from "../../elements";
import FollowerCard from "../FollowerCard";
import styles from "./index.module.scss";

const AsideFollower = () => {
  const [tab, setTab] = React.useState(0);
  const [results, setResults] = React.useState<some | null>(null);
  const [filter, setFilter] = React.useState<some>({ page: 1, pageSize: 20 });
  const [loading, setLoading] = React.useState(false);

  const getData = React.useCallback(async () => {
    setLoading(true);
    const res =
      tab === 0 ? await getSearchResult(filter) : await getFollowing(filter);
    if (res?.status === 200) {
      setResults((old) => ({
        total: res.data?.total,
        data: [...(old?.data || []), ...res?.data?.data],
      }));
    }
    setLoading(false);
  }, [filter, tab]);

  const handleChange = (e: any, val: number) => {
    setTab(val);
    setResults(null);
    setFilter({ page: 1, pageSize: 20 });
  };

  React.useEffect(() => {
    getData();
  }, [getData]);

  return (
    <>
      <div className={styles.main} id="scrollParent">
        <Tabs
          variant="fullWidth"
          className={styles.tab}
          value={tab}
          onChange={handleChange}
        >
          <Tab label="Followers" />
          <Tab label="Following" />
        </Tabs>
        <InfiniteScroll
          dataLength={results?.data?.length || 0}
          next={() => setFilter({ ...filter, page: filter.page + 1 })}
          hasMore={results?.data?.length < results?.total}
          loader={<div>loading...</div>}
          scrollableTarget="scrollParent"
          style={{ overflow: "hidden", marginTop: 8 }}
        >
          {results?.data?.map((item: some) => (
            <FollowerCard key={item.id} data={item} />
          ))}
          {loading &&
            Array(4)
              .fill({ skeleton: true })
              .map((item: some, index: number) => (
                <FollowerCard key={index} data={item} />
              ))}
        </InfiniteScroll>
      </div>
    </>
  );
};

export default AsideFollower;
