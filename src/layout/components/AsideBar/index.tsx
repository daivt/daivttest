import { ButtonBase, Typography } from "@mui/material";
import React from "react";
import { useLocation } from "react-router";
import { Link } from "react-router-dom";
import { LogoIcon, MenuActive, MenuInactive } from "../../../assets";
import { listRouteLabel } from "../../../routers";
import { Col } from "../../elements";
import styles from "./index.module.scss";

const AsideBar = () => {
  const location = useLocation();

  return (
    <>
      <div className={styles.divContent}></div>
      <Col className={styles.mainBar}>
        <ButtonBase className={styles.logoIcon}>
          <LogoIcon />
        </ButtonBase>
        {listRouteLabel.map((route, index) => {
          const isActive =
            location.pathname === route.path ||
            route?.subMenu?.some((v) => v.path === location.pathname);
          return (
            <Link
              to={route.path}
              key={index}
              style={{ textDecoration: "none" }}
            >
              <ButtonBase className={styles.menuItem}>
                <Col className={styles.colButton}>
                  {isActive ? <MenuActive /> : <MenuInactive />}
                  {isActive && (
                    <Typography variant="caption">{route.title} </Typography>
                  )}
                </Col>
              </ButtonBase>
            </Link>
          );
        })}
      </Col>
    </>
  );
};

export default AsideBar;
