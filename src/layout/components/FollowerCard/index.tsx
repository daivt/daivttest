import { Avatar, Button, Skeleton, Typography } from "@mui/material";
import React from "react";
import { Col, Row, some } from "../../elements";
import styles from "./index.module.scss";

interface Props {
  data: some;
}

const FollowerCard: React.FC<Props> = (props) => {
  const { data } = props;
  if (data.skeleton) {
    return (
      <Row className={styles.main}>
        <Row className={styles.info}>
          <Avatar
            src={data.avater}
            alt={""}
            className={styles.avatar}
            variant="rounded"
          />
          <Col>
            <Typography variant="body2">
              <Skeleton width="80px" />
            </Typography>
            <Typography variant="body2" style={{ opacity: 0.5 }}>
              <Skeleton width="120px" />
            </Typography>
          </Col>
        </Row>
        <Skeleton
          variant="rectangular"
          style={{
            width: 80,
            height: 28,
            borderRadius: 14,
          }}
        />
      </Row>
    );
  }
  return (
    <Row className={styles.main} onClick={() => console.log(data?.name)}>
      <Row className={styles.info}>
        <Avatar
          src={data.avater}
          alt={""}
          className={styles.avatar}
          variant="rounded"
        />
        <Col>
          <Typography variant="body2">{data?.name || <Skeleton />}</Typography>
          <Typography variant="body2" style={{ opacity: 0.5 }}>
            {data?.username || <Skeleton />}
          </Typography>
        </Col>
      </Row>
      <Button
        variant={data?.isFollowing ? "contained" : "outlined"}
        size="small"
      >
        {data?.isFollowing ? "Following" : "Follow"}
      </Button>
    </Row>
  );
};

export default FollowerCard;
