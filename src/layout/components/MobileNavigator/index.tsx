import { ButtonBase } from "@mui/material";
import React from "react";
import { Link, useLocation } from "react-router-dom";
import { MenuActive, MenuInactive } from "../../../assets";
import { listRoute, listRouteLabel } from "../../../routers";
import { Row } from "../../elements";
import styles from "./index.module.scss";

const MobileNavigator = () => {
  const location = useLocation();
  return (
    <Row className={styles.main}>
      {listRouteLabel.map((route, index) => {
        const isActive =
          location.pathname === route.path ||
          route?.subMenu?.some((v) => v.path === location.pathname);
        return (
          <Link
            to={route.path}
            key={index}
            style={{ textDecoration: "none" }}
            className={
              index < listRoute.length - 1 ? styles.colButton : undefined
            }
          >
            <ButtonBase>
              {isActive ? <MenuActive /> : <MenuInactive />}
            </ButtonBase>
          </Link>
        );
      })}
    </Row>
  );
};

export default MobileNavigator;
