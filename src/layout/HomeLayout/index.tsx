import { useMediaQuery } from "@mui/material";
import React from "react";
import { Outlet } from "react-router-dom";
import { MOBILE_WIDTH } from "../../constants";
import AsideFollower from "../components/AsideFollower";
import MobileNavigator from "../components/MobileNavigator";
import { Col, Row } from "../elements";
import styles from "./index.module.scss";
const HomeLayout = () => {
  const matchFollower = useMediaQuery("(min-width:1400px)");

  const isMobile = useMediaQuery(MOBILE_WIDTH);
  return (
    <>
      <Row className={styles.wrapper}>
        <Col className={styles.main}>
          <Outlet />
        </Col>
        {matchFollower && <AsideFollower />}
        {isMobile && <MobileNavigator />}
      </Row>
    </>
  );
};

export default HomeLayout;
