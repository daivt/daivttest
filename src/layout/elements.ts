import styled from "styled-components";

export type some = { [key: string]: any };

export const Row = styled.div`
  display: flex;
`;

export const Col = styled.div`
  display: flex;
  flex-direction: column;
`;
