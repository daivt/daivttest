import { useMediaQuery } from "@mui/material";
import React from "react";
import { useRoutes } from "react-router-dom";
import { MOBILE_WIDTH } from "../../constants";
import { listRoute } from "../../routers";
import AsideBar from "../components/AsideBar";
import MobileHeader from "../components/MobileHeader";
import { Col, Row } from "../elements";
import styles from "./index.module.scss";

const DefaultLayout = () => {
  const isMobile = useMediaQuery(MOBILE_WIDTH);

  let element = useRoutes(listRoute);

  return (
    <>
      <Col style={{ position: "relative" }}>
        {isMobile && <MobileHeader />}
        <Row className={styles.mainLayout}>
          {!isMobile && <AsideBar />}
          <Col className={styles.route}>{element}</Col>
        </Row>
      </Col>
    </>
  );
};

export default DefaultLayout;
