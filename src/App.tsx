// @ts-ignore
import React from "react";
import "./App.scss";
import DefaultLayout from "./layout/DefaultLayout";

const App = () => {
  return <DefaultLayout />;
};

export default App;
